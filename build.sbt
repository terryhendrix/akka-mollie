import sbt._
import Build._

lazy val mollie = (project in file("."))
  .dependsOn(Projects.peanuts)
  .settings(
    organization := "nl.bluesoft",
    name := "akka-mollie",
    version := "1.0.0",
    scalaVersion := "2.11.6",
    resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository",
    credentials += Credentials(Path.userHome / ".ivy2" / ".credentials"),
    libraryDependencies ++= Seq(
      "org.scala-lang"           % "scala-library"                  % scalaVersion.value,
      "com.typesafe.akka"       %% "akka-kernel"                    % V.akkaV,
      "com.typesafe.akka"       %% "akka-actor"                     % V.akkaV,
      "com.typesafe.akka"       %% "akka-slf4j"                     % V.akkaV,
      "com.typesafe.akka"       %% "akka-cluster"                   % V.akkaV,
      "com.typesafe.akka"       %% "akka-contrib"                   % V.akkaV,
      "io.spray"                %% "spray-http"                     % V.sprayV,
      "io.spray"                %% "spray-httpx"                    % V.sprayV,
      "io.spray"                %% "spray-routing"                  % V.sprayV,
      "io.spray"                %% "spray-util"                     % V.sprayV,
      "io.spray"                %% "spray-io"                       % V.sprayV,
      "io.spray"                %% "spray-can"                      % V.sprayV,
      "io.spray"                %% "spray-client"                   % V.sprayV,
      "io.spray"                %% "spray-json"                     % V.jsonV,
      "com.typesafe.akka"       %% "akka-testkit"                   % V.akkaV     % "test",
      "org.scalatest"           %% "scalatest"                      % "2.1.4"   % "test",
      "joda-time"               %  "joda-time"                       % "2.3",
      "org.joda"                %  "joda-convert"                    % "1.6"
    ),
    autoCompilerPlugins := true,
    scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8"),
    publishMavenStyle := true,
    publishArtifact in Test := false,
    fork := true,
    parallelExecution in test := false
  )
