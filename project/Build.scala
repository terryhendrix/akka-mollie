import sbt._
object Build {
  object V {
    val akkaV = "2.3.6"
    val sprayV = "1.3.1"
    val jsonV = "1.2.6"
    val peanuts = "1.0.5"      // Dont upgrade peanuts, binary incompatability
  }

  object Projects {
    lazy val peanuts = RootProject(uri("https://bitbucket.org/terryhendrix/peanuts.git#"+V.peanuts))
  }
}