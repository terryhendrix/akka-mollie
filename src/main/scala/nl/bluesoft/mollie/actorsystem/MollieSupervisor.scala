package nl.bluesoft.mollie.actorsystem
import akka.actor.{ActorRef, Props, Actor, ActorLogging}
import nl.bluesoft.mollie.Mollie._
import nl.bluesoft.mollie.{MollieConfig, Mollie}
import java.util.UUID
import nl.bluesoft.peanuts.http.HttpClient
import nl.bluesoft.mollie.Mollie.Unauthorized
import nl.bluesoft.mollie.Mollie.MakePaymentCommand
import nl.bluesoft.mollie.Mollie.GetPayments
import nl.bluesoft.mollie.Mollie.PaymentCreated
import spray.http.StatusCodes
import nl.bluesoft.peanuts.json.JsonMarshalling

object MollieSupervisor {
  def props(config:MollieConfig, webhook:Webhook) = Props(new MollieSupervisor(config,webhook))
}

class MollieSupervisor(config:MollieConfig, webhook:Webhook) extends Actor with ActorLogging with JsonMarshalling with JsonFormats
{
  implicit val ec = context.system.dispatcher
  val http = HttpClient(context.system.settings.config.getConfig("mollie.client"))(context.system, log)
  var unhandled:Map[String, ActorRef] = Map.empty
  var recoverCallback:Option[Mollie.Payment => Unit] = None

  override def preStart() = webhook.registerSupervisor(self)
  override def postStop() = webhook.unregisterSupervisor(self)

  override def receive: Actor.Receive = {
    case cmd @ MakePaymentCommand(_,_) =>
      val paymentHandler = context.actorOf(PaymentHandler.props(config), "payment-"+UUID.randomUUID().toString)
      paymentHandler.forward(cmd.copy(callback = cmd.callback.orElse(recoverCallback)))

    case GetPayments(count, offset) =>
      val ref = sender()
      http.get(
        resource = Resources.payments,
        params = Option(Map("offset" -> offset.toString, "count" -> count.toString)),
        headers = Mollie.authorization(config.apiKey)
      ).map { response =>
        response.status match {
          case StatusCodes.OK =>
            ref ! response.entity.asString.asObject[Page[Payment]](paymentPageFormat)

          case statusCode =>
            log.warning(s"Unhandled $statusCode")
        }
      }.recover { case ex =>
        ex.printStackTrace()
      }

    case RecoverPayment(id) =>
      val ref = context.actorOf(PaymentHandler.props(config), "payment-"+id)
      webhook.registerPayment(id, ref)
      recoverCallback.map { callback =>
        ref ! UpdatePayment(id, recoverCallback)
        sender ! Recovered(id)
      }.getOrElse {
        unhandled = unhandled + (id -> ref)
        sender ! RecoveryFailed(id)
      }

    case RecoverPayments(callback) =>
      recoverCallback = Some(callback)
      unhandled.map(u => u._2 ! UpdatePayment(u._1, Some(callback)))
      unhandled = Map.empty

    case GetIssuers(count, offset) =>
      val ref = sender()
      http.get(
        resource = Resources.issuers,
        params = Option(Map("offset" -> offset.toString, "count" -> count.toString)),
        headers = Mollie.authorization(config.apiKey)
      ).map { response =>
        response.status match {
          case StatusCodes.OK =>
            ref ! response.entity.asString.asObject[Page[Issuer]](issuerPageFormat)

          case statusCode =>
            log.warning(s"Unhandled $statusCode")
        }
      }.recover { case ex =>
        ex.printStackTrace()
      }

    case error:Unauthorized =>
      log.error(s"An error occurred: ${error.error.message}")

    case error:Mollie.Error =>
      self ! Unauthorized(error)

    case created:PaymentCreated =>
      webhook.registerPayment(created.payment.id, sender())


    case any => log.warning(s"Unexpected: $any")

  }
}
