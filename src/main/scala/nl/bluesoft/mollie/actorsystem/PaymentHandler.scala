package nl.bluesoft.mollie.actorsystem
import akka.actor._
import nl.bluesoft.peanuts.http.HttpClient
import nl.bluesoft.mollie.Mollie._
import nl.bluesoft.peanuts.json.JsonMarshalling
import nl.bluesoft.mollie.{MollieConfig, Mollie}
import spray.http.StatusCodes
import nl.bluesoft.mollie.Mollie.Unauthorized
import scala.Some
import scala.concurrent.duration._

/**
 * Created by terryhendrix on 09/07/15.
 */
object PaymentHandler
{

  def props(config:MollieConfig) = Props(new PaymentHandler(config))
}

class PaymentHandler(config:MollieConfig) extends Actor with ActorLogging with JsonMarshalling with Mollie.JsonFormats
{
  var state:Option[Mollie.Payment] = None
  var callback:Option[Mollie.Payment => Unit] = None

  val http = HttpClient(context.system.settings.config.getConfig("mollie.client"))(context.system, log)

  implicit val ec = context.system.dispatcher

  override def receive: Actor.Receive = {
    case MakePaymentCommand(_callback, command) =>
      val ref = sender()
      callback = _callback

      http.post(
        resource = Resources.payments,
        body = Some(command.asJson),
        headers = Mollie.authorization(config.apiKey)
      ).map { response =>
        response.status match {
          case StatusCodes.Unauthorized =>
            val error = response.entity.asString.asObject[Unauthorized]
            context.parent ! error
            ref ! error

          case StatusCodes.Created =>
            val payment = response.entity.asString.asObject[Mollie.Payment]
            context.setReceiveTimeout(payment.expiryPeriod.map(ISODuration(_)).getOrElse(15 minutes))
            state = Some(payment)
            ref ! PaymentCreated(payment)
            context.parent ! PaymentCreated(payment)

          case statusCode =>
            log.warning(s"Unhandled $statusCode")
        }
      }.recover { case ex =>
        ex.printStackTrace()
      }

    case UpdatePayment(id, _callback) =>

      http.get(
        resource = Resources.payments(id),
        headers = Mollie.authorization(config.apiKey)
      ).map { response =>
        response.status match {
          case StatusCodes.OK =>
            val payment = response.entity.asString.asObject[Mollie.Payment]
            state = Some(payment)
            callback.orElse(_callback).map(call => call(payment)).getOrElse(log.info("There is no callback registered"))

          case statusCode =>
            log.warning(s"Unhandled $statusCode")
        }
      }.recover { case ex =>
        ex.printStackTrace()
      }

    case ReceiveTimeout =>
      self ! PoisonPill

    case any => log.warning(s"Unexpected: $any")
  }
}
