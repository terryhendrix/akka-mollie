package nl.bluesoft.mollie.actorsystem
import akka.pattern._
import spray.routing.{Route, SimpleRoutingApp}
import akka.actor._
import nl.bluesoft.mollie.Mollie._
import spray.http.StatusCodes
import akka.event.{Logging, LoggingAdapter}
import nl.bluesoft.mollie.WebhookConfig
import nl.bluesoft.mollie.actorsystem.Webhook.Unregister
import scala.concurrent.duration._
import akka.util.Timeout
import nl.bluesoft.mollie.actorsystem.Webhook.Get
import nl.bluesoft.mollie.actorsystem.Webhook.Unregister
import nl.bluesoft.mollie.Mollie.RecoverPayment
import nl.bluesoft.mollie.Mollie.UpdatePayment
import nl.bluesoft.mollie.actorsystem.Webhook.Register
import nl.bluesoft.mollie.actorsystem.Webhook.Get
import scala.Some
import spray.httpx.marshalling.ToResponseMarshallable
import scala.concurrent.Future

/**
 * Created by terryhendrix on 10/07/15.
 */
object Webhook {
  case class Register(id:String, ref:ActorRef)
  case class Unregister(id:String)
  case class Get(id:String)
}

class Webhook(config:WebhookConfig)(implicit system:ActorSystem) extends SimpleRoutingApp with JsonFormats
{
  implicit val timeout = Timeout(5 seconds)
  implicit val ec = system.dispatcher
  private val log:LoggingAdapter = Logging(system, "webhook")
  val register = system.actorOf(Props(new Registering), "registering")
  private var supervisor:Option[ActorRef] = None

  startServer(interface = config.interface, port = config.port, serviceActorName = "mollie-webhook") { webhookRoute }

  lazy val webhookRoute:Route = {
    pathPrefix("webhook") {
      entity(as[String]) { params =>
        complete {
          log.info(s"Webhook called with params $params")
          getPaymentId(params) match  {
            case Some(paymentId) =>
              (register ? Get(paymentId)).flatMap[ToResponseMarshallable] {
                  case Some(paymentHandler:ActorRef) =>
                    paymentHandler ! UpdatePayment(paymentId)
                    Future(StatusCodes.Accepted)
                  case None =>
                    log.warning(s"There is no actor registered for $paymentId. Attempting recover.")
                    supervisor.map[Future[ToResponseMarshallable]] { supervisor =>
                      (supervisor ? RecoverPayment(paymentId)).map {
                        case Recovered(id) =>
                          log.info(s"Recovered payment with '$id'")
                          StatusCodes.OK
                        case RecoveryFailed(id) =>
                          log.warning(s"Recovery of payment '$id; failed. Did you register a callback using the 'Mollie.recoverPaymentsWith(Payment => Unit)'")
                          StatusCodes.InternalServerError
                      }
                    }.getOrElse {
                      log.warning(s"There is no MollieSupervisor registered.'")
                      Future(StatusCodes.InternalServerError)
                    }
              }
            case None =>
              log.error(s"The paymentId is not supplied as post parameter 'id' in $params")
              StatusCodes.InternalServerError
          }
        }
      }
    }
  }

  def getPaymentId(params:String) = {
    params.split(" ").find( _.startsWith("id=")).map(_.split("=")(1))
  }

  def registerSupervisor(supervisor:ActorRef) = {
    log.info(s"Registering supervisor $supervisor")
    this.supervisor = Some(supervisor)
  }

  def unregisterSupervisor(supervisor:ActorRef) = {
    if(this.supervisor.exists(_ == supervisor)){
      this.supervisor = None
      log.info(s"Unregistering supervisor $supervisor")
    }
  }

  def registerPayment(id:String, paymentHandler:ActorRef) = {
    log.info(s"Registering payment $id as $paymentHandler")
    register ! Register(id, paymentHandler)
  }


  def unregisterPayment(id:String) = {
    log.info(s"Registering payment $id")
    register ! Unregister(id)
  }




  class Registering extends Actor with ActorLogging {
    private var registered:Map[String,ActorRef] = Map.empty
    override def receive: Actor.Receive = {
      case Register(id,paymentHandler) =>
        registered = registered + (id -> paymentHandler)

      case Unregister(id) =>
        registered = registered - id

      case Get(id) =>
        sender ! registered.find(_._1 == id).map(_._2)
    }
  }
}
