package nl.bluesoft.mollie

import com.typesafe.config.Config
import akka.actor.ActorSystem
import scala.concurrent.duration.{FiniteDuration, Duration}
import java.util.concurrent.TimeUnit

object MollieConfig
{
  def apply(system:ActorSystem):MollieConfig = {
    apply(system.settings.config.getConfig("mollie"))
  }

  def apply(config:Config):MollieConfig = {
    new MollieConfig(
      WebhookConfig(config.getConfig("webhook")),
      config.getString("api-key")
    )
  }
}

case class MollieConfig(webhook:WebhookConfig, apiKey:Mollie.ApiKey)

case object WebhookConfig {
  def apply(system:ActorSystem):WebhookConfig = {
    apply(system.settings.config.getConfig("mollie.webhook"))
  }

  def apply(config:Config):WebhookConfig = {
     new WebhookConfig(
       config.getString("base-url"),
       config.getString("interface"),
       config.getInt("port")
     )
  }
}

case class WebhookConfig(baseUrl:String, interface:String, port:Int)


trait ExtendedConfig {
  implicit class RichConfig(config:Config) {
    implicit def getFiniteDuration(key:String) = {
      val d = Duration(config.getString(key))
      FiniteDuration(d.toMillis, TimeUnit.MILLISECONDS)
    }
  }
}
