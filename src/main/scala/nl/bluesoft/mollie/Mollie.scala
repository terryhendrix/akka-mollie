package nl.bluesoft.mollie
import akka.actor.{ExtensionIdProvider, Extension, ExtendedActorSystem, ExtensionId}
import nl.bluesoft.mollie.actorsystem.{Webhook, MollieSupervisor}
import akka.util.Timeout
import akka.pattern._
import scala.concurrent.Future
import spray.json.DefaultJsonProtocol
import spray.httpx.marshalling.MetaMarshallers
import scala.concurrent.duration._

class MollieException(message:String) extends Exception(message)

object Mollie extends ExtensionId[Mollie] with ExtensionIdProvider {
  override def createExtension(system: ExtendedActorSystem): Mollie = new MollieImpl(system, MollieConfig(system))
  override def lookup(): ExtensionId[_ <: Extension] = Mollie
  val DEFAULT_LIMIT = 1000

  /**
   * A trait for the actor system commands
   */
  trait Command
  case class MakePaymentCommand(callback:Option[Payment => Unit], makePayment:MakePayment) extends Mollie.Command
  case class RecoverPayment(paymentId:String) extends Mollie.Command
  case class RecoverPayments(callback:Payment => Unit) extends Mollie.Command
  case class UpdatePayment(paymentId:String, callback: Option[Payment => Unit] = None) extends Mollie.Command
  case class GetPayments(count:Count, offset:Offset) extends Mollie.Command
  case class GetIssuers(count:Count, offset:Offset) extends Mollie.Command

  /**
   * A trait for the Mollie REST API commands
   */
  trait ApiCommand
  case class MakePayment(amount: Double, description: String, redirectUrl: String, issuer:Option[IssuerId] = None,
                         method: Option[Mollie.PaymentMethod] = None, metadata: Option[Map[String, String]] = None,
                         webhookUrl: Option[Mollie.Url] = None, locale: Option[Mollie.Locale] = None) extends Mollie.ApiCommand {
    if(issuer.isDefined)
      require(method.exists(_ == PaymentMethods.ideal), "The method must be ideal when an issuer is specified")
  }


  trait Event
  case class PaymentCreated(payment:Mollie.Payment) extends Mollie.Event
  case class Unauthorized(error:Error) extends Mollie.Event
  case class Recovered(id:String) extends Mollie.Event
  case class RecoveryFailed(id:String) extends Mollie.Event

  type ApiKey = String

  type PaymentMode = String

  def authorization(apiKey:String) = Map("Authorization" -> s"Bearer $apiKey")

  type Url = String

  type Resource = String
  case object Resources {
     val payments:String = "payments"
     val issuers:String = "issuers"
     def payments(id:String):String = s"payments/$id"
  }

  case object PaymentModes {
    val test:PaymentMode = "test"
    val live:PaymentMode = "live"

    val valid = Seq(test, live)
  }

  type PaymentStatus = String
  case object PaymentStatuses {
    val open:PaymentStatus = "open"
    val cancelled:PaymentStatus = "cancelled"
    val expired:PaymentStatus = "expired"
    val pending:PaymentStatus = "pending"
    val paid:PaymentStatus = "paid"
    val paidout:PaymentStatus = "paidout"
    val refunded:PaymentStatus = "refunded"

    val valid = Seq(open, cancelled, expired, pending, paid, paidout, refunded)
  }

  type PaymentMethod = String
  case object PaymentMethods {
    val ideal:PaymentMethod = "ideal"
    val creditcard:PaymentMethod = "creditcard"
    val mistercash:PaymentMethod = "mistercash"
    val belfius:PaymentMethod = "belfius"
    val sofort:PaymentMethod = "sofort"
    val banktransfer:PaymentMethod = "banktransfer"
    val paypal:PaymentMethod = "paypal"
    val bitcoin:PaymentMethod = "bitcoin"
    val paysafecard:PaymentMethod = "paysafecard"

    val valid = Seq(ideal, creditcard, mistercash, belfius, sofort, banktransfer, paypal, bitcoin, paysafecard)
  }

  type Locale = String
  case object Locales {
    val de:Locale = "de"
    val en:Locale = "es"
    val es:Locale = "es"
    val fr:Locale = "fr"
    val be:Locale = "be"
    val be_fr:Locale = "be-fr"
    val nl:Locale = "nl"

    val valid = Seq(de, en, es, fr, be, be_fr, nl)
  }


  type Offset = Int
  type Count = Int
  type TotalCount = Int

  case class PageLinks(first:Url, last:Url, previous:Option[Url], next:Option[Url])
  case class Page[U <: PageItem](data:Seq[U], count:Count, offset:Offset, totalCount:TotalCount, links:Option[PageLinks])

  trait PageItem

  case class Payment (
      id:String, mode:PaymentMode, createdDatetime:String, status:PaymentStatus,
      paidDateTime:Option[String], cancelledDateTime:Option[String],
      expiredDateTime:Option[String], expiryPeriod:Option[String],
      amount:String, amountRefunded:Option[String], amountRemaining:Option[String],
      description:String, method:Option[PaymentMethod], metadata:Option[Map[String, String]],
      locale:Option[Locale], details:Option[Map[String,String]], profileId:String,
      links:Map[String,String]) extends PageItem {
    require(PaymentModes.valid.contains(mode), s"mode must be one of ${PaymentModes.valid.mkString(", ")}")
    require(PaymentStatuses.valid.contains(status), s"status must be one of ${PaymentStatuses.valid.mkString(", ")}")
    require(method.map(m => PaymentMethods.valid.contains(m)).getOrElse(true), s"method must be one of ${PaymentMethods.valid.mkString(", ")}")
  }

  type IssuerId = (String)

  case class Issuer (id:IssuerId, name:String, method:PaymentMethod) extends PageItem {
    require(PaymentMethods.valid.contains(method), s"method must be one of ${PaymentMethods.valid.mkString(", ")}")
  }

  /**
   * Duration according to https://en.wikipedia.org/wiki/ISO_8601#Durations
   */
  object ISODuration {
    def apply(isoDuration:String):Duration = {
      if(timeOnly(isoDuration)) {
        Duration(timeToDurationString(isoDuration.replace("PT", "")))
      }
      else
        throw new RuntimeException(s"Could not parse $isoDuration")
    }

    def timeToDurationString(time:String) = {
      time.replace("S", " seconds ").replace("M", " minutes ").replace("H", " hours ").trim
    }

    def timeOnly(isoDuration:String) = isoDuration.contains("PT")
  }

  /**
   *  {
   *    "error": {
   *      "type":"request",
   *      "message":"Unauthorized request",
   *      "links":{
   *        "documentation":"https://www.mollie.com/en/docs"
   *      }
   *    }
   *  }
   */
  case class Error(message:String, links:Map[String,String])

  trait JsonFormats extends DefaultJsonProtocol with MetaMarshallers
  {
    implicit val makePaymentFormat = jsonFormat8(MakePayment)
    implicit val errorFormat = jsonFormat2(Mollie.Error)
    implicit val mollieErrorFormat = jsonFormat1(Unauthorized)
    implicit val paymentFormat = jsonFormat18(Mollie.Payment)
    implicit val issuerFormat = jsonFormat3(Mollie.Issuer)
    implicit val pageLinksFormat = jsonFormat4(Mollie.PageLinks)
    implicit val paymentPageFormat = jsonFormat5(Mollie.Page[Payment])
    implicit val issuerPageFormat = jsonFormat5(Mollie.Page[Issuer])
  }


  def webhookUrl(config:MollieConfig):Mollie.Url = {
    if(config.webhook.baseUrl.endsWith("/"))
      s"${config.webhook.baseUrl}:${config.webhook.port}webhook"
    else
      s"${config.webhook.baseUrl}:${config.webhook.port}/webhook"
  }
}

trait Mollie extends Extension
{
  import Mollie._

  protected def webhookUrl:Mollie.Url = Mollie.webhookUrl(config)

  def config:MollieConfig

  def paymentWithCallback(amount:Double,description:String, redirectUrl:String,
                  method:Option[Mollie.PaymentMethod] = None, metadata:Option[Map[String, String]] = None,
                  locale:Option[Mollie.Locale] = None, issuer:Option[IssuerId] = None)
                 (callback:Payment => Unit)
                 (implicit timeout:Timeout) : Future[Mollie.Payment]

  def payment(amount:Double,description:String, redirectUrl:String,
              method:Option[Mollie.PaymentMethod] = None, metadata:Option[Map[String, String]] = None,
              locale:Option[Mollie.Locale] = None, issuer:Option[IssuerId] = None)
             (implicit timeout:Timeout) : Future[Mollie.Payment]

  def payments(count:Int = 100, offset:Int = 0)(implicit timeout:Timeout) : Future[Page[Payment]]

  def handlePaymentsWith(callback:Payment => Unit):Unit

  def issuers(count:Count, offset:Offset)(implicit timeout:Timeout) : Future[Page[Issuer]]
}

class MollieImpl(system:ExtendedActorSystem, override val config:MollieConfig) extends Mollie
{
  import Mollie._

  implicit val ec = system.dispatcher
  val webhook = new Webhook(config.webhook)(system)
  val supervisor = system.actorOf(MollieSupervisor.props(config, webhook), "payments")

  override def paymentWithCallback(amount:Double,description:String, redirectUrl:String,
                      method:Option[Mollie.PaymentMethod], metadata:Option[Map[String, String]],
                      locale:Option[Mollie.Locale], issuer:Option[IssuerId] = None)
                      (callback:Payment => Unit)(implicit timeout:Timeout): Future[Mollie.Payment] = {
    val f = supervisor ? MakePaymentCommand(Some(callback), MakePayment(
      amount = amount,
      description = description,
      redirectUrl = redirectUrl,
      method = method,
      issuer = issuer,
      metadata = metadata,
      webhookUrl = Some(webhookUrl),
      locale = locale)
    )
    f.map {
      case created:PaymentCreated   =>  created.payment
      case error:Unauthorized        =>  throw new MollieException(error.error.message)
    }
  }

  override def payment(amount:Double,description:String, redirectUrl:String,
              method:Option[Mollie.PaymentMethod] = None, metadata:Option[Map[String, String]] = None,
              locale:Option[Mollie.Locale] = None, issuer:Option[IssuerId] = None) (implicit timeout:Timeout) : Future[Mollie.Payment] = {
    val f = supervisor ? MakePaymentCommand(None, MakePayment(
      amount = amount,
      description = description,
      redirectUrl = redirectUrl,
      method = method,
      issuer = issuer,
      metadata = metadata,
      webhookUrl = Some(webhookUrl),
      locale = locale)
    )
    f.map {
      case created:PaymentCreated   =>  created.payment
      case error:Unauthorized        =>  throw new MollieException(error.error.message)
    }
  }

  def payments(count:Count, offset:Offset)(implicit timeout:Timeout) : Future[Page[Payment]] = {
    val f = supervisor ? GetPayments(count, offset)
    f.map {
      case payments:Page[Payment] => payments
    }
  }

  def handlePaymentsWith(callback:Payment => Unit):Unit = {
    supervisor ! RecoverPayments(callback)
  }

  override def issuers(count:Count, offset:Offset)(implicit timeout: Timeout): Future[Page[Issuer]] = {
    val f = supervisor ? GetIssuers(count,offset)
    f.map {
      case issuers:Page[Issuer] => issuers
    }
  }
}
