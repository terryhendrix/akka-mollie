package nl.bluesoft.mollie.spec
import org.scalatest._
import akka.actor.ActorSystem
import nl.bluesoft.mollie.{MollieConfig, Mollie}
import org.scalatest.concurrent.{Eventually, Futures}
import scala.concurrent.duration._
import akka.util.Timeout
import scala.sys.process._
import scala.concurrent.{Await, Future}
import org.scalatest.time.{Seconds, Span}
import nl.bluesoft.peanuts.http.HttpClient
import nl.bluesoft.peanuts.json.JsonMarshalling
import nl.bluesoft.mollie.Mollie.JsonFormats

class TestSpec extends FeatureSpec with GivenWhenThen with Eventually with Matchers with JsonFormats with JsonMarshalling
               with Futures with BeforeAndAfterAll with BeforeAndAfterEach
{

  val system = ActorSystem("MollieTest")
  val mollie = Mollie(system)
  val config = MollieConfig(system)
  val http = HttpClient(system.settings.config.getConfig("mollie.client"))(system, system.log)
  val browserBinary = system.settings.config.getString("test.browser-binary")
  implicit val ec = system.dispatcher

  implicit val timeout = Timeout(5 seconds)

  implicit def uToOption[U](u:U)=Option(u)

  implicit class FutureValue[U](f:Future[U]) {
    def futureValue(implicit duration:FiniteDuration = timeout.duration):U = Await.result(f, duration)
  }

  def lapseTime(implicit duration:FiniteDuration = timeout.duration) = Thread.sleep(duration.toMillis)

  def openUrl(url:String) = s"$browserBinary $url".!!

  override implicit val patienceConfig  = PatienceConfig(scaled(Span(60, Seconds)), scaled(Span(1, Seconds)))
}
