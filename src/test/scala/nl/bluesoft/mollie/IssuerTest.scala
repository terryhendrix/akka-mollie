package nl.bluesoft.mollie

import nl.bluesoft.mollie.spec.TestSpec
import nl.bluesoft.mollie.Mollie.{PaymentMethods, MakePayment, Resources, PaymentStatuses}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

class IssuerTest extends TestSpec
{
  feature("Payments must be made on the mollie api with a specific issuer") {
    scenario("1. Create a payment with a predefined issuer") {
      Given("Some callback is defined by the user to handle the payment status changes")
      var payment:Option[Mollie.Payment] = None

      val issuers = mollie.issuers(100, 0).futureValue

      val callback:Mollie.Payment => Unit = { _payment =>
        system.log.info(s"Callback ${_payment.id} to test")
        payment = Some(_payment)
      }

      When("A payment is made")
      val created = mollie.paymentWithCallback(1.00, "test", "http://31.201.130.32", method = PaymentMethods.ideal, issuer = issuers.data.head.id)(callback).futureValue

      Then("We get a paymentUrl, a webhookUrl and a description back")
      created.links("paymentUrl") should startWith("https://www.mollie.com")
      created.links("webhookUrl") should startWith("http://31.201.130.32:8080/webhook")
      created.description shouldBe "test"

      When("The customer is redirected and pays")
      openUrl(created.links("paymentUrl"))

      Then("The callback must fire")
      eventually {
        payment should not be None
        payment.get.status shouldBe PaymentStatuses.paid
      }
    }
  }
}
