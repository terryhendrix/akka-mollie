package nl.bluesoft.mollie
import nl.bluesoft.mollie.spec.TestSpec
import nl.bluesoft.mollie.Mollie.{MakePayment, Resources, PaymentStatuses}
import scala.concurrent.Future
import scala.concurrent.duration._

class PaymentTest extends TestSpec
{
  feature("Payments must be made on the mollie api") {
    scenario("1. Create a payment should handled with callback") {
      Given("Some callback is defined by the user to handle the payment status changes")
      var payment:Option[Mollie.Payment] = None
      val callback:Mollie.Payment => Unit = { _payment =>
        system.log.info(s"Callback ${_payment.id} to test")
        payment = Some(_payment)
      }

      When("A payment is made")
      val created = mollie.paymentWithCallback(1.00, "test", "http://31.201.130.32")(callback).futureValue

      Then("We get a paymentUrl, a webhookUrl and a description back")
      created.links("paymentUrl") should startWith("https://www.mollie.com")
      created.links("webhookUrl") should startWith("http://31.201.130.32:8080/webhook")
      created.description shouldBe "test"

      When("The customer is redirected and pays")
      openUrl(created.links("paymentUrl"))

      Then("The callback must fire")
      eventually {
        payment should not be None
        payment.get.status shouldBe PaymentStatuses.paid
      }
    }

    scenario("2. It should recover payments, the callback is registerd after posting payments") {
      Given("2 payments are created by mollie")
      val payments = Future.sequence(Seq(1,2).map[Future[Mollie.Payment], Seq[Future[Mollie.Payment]]] { _ =>
        http.post(
          resource = Resources.payments,
          body = MakePayment(
            amount = 1.00,
            description = "test-recover",
            redirectUrl = "http://31.201.130.32",
            webhookUrl = Mollie.webhookUrl(config)
          ).asJson,
          headers = Mollie.authorization(config.apiKey)
        ).map(_.entity.asString.asObject[Mollie.Payment])
      }).futureValue

      var remainingIDs = payments.map(_.id)
      openUrl(payments.head.links("paymentUrl"))

      And("A certian call back is registered to recover the payments")
      mollie.handlePaymentsWith{ payment =>
        system.log.info(s"Callback ${payment.id} to test recover (registered before payments)")
        if(remainingIDs.exists(_ == payment.id)) {
          remainingIDs = remainingIDs.filter(_ != payment.id)
          payments.find(_.id == remainingIDs.headOption.getOrElse("testing is done")).map { payment =>
            openUrl(payment.links("paymentUrl"))
          }
        }
      }

      When("The customers pay the 2 payments")
      Then("The payments should be recovered")
      eventually {
        remainingIDs shouldBe empty  // all id's should have been handled
      }
    }


    scenario("3. It should recover payments, the callback is registerd before posting payments") {
      Given("A certian call back is registered to recover the payments")
      var remainingIDs = Seq.empty[String]
      var payments = Seq.empty[Mollie.Payment]

      mollie.handlePaymentsWith{ payment =>
        system.log.info(s"Callback ${payment.id} to test recover (registered before payments)")
        if(remainingIDs.exists(_ == payment.id)) {
          remainingIDs = remainingIDs.filter(_ != payment.id)
          payments.find(_.id == remainingIDs.headOption.getOrElse("testing is done")).map { payment =>
            openUrl(payment.links("paymentUrl"))
          }
        }
      }

      And("2 payments are created by mollie")
      payments = Future.sequence(Seq(1,2).map[Future[Mollie.Payment], Seq[Future[Mollie.Payment]]] { _ =>
        http.post(
          resource = Resources.payments,
          body = MakePayment(
            amount = 1.00,
            description = "test-recover",
            redirectUrl = "http://31.201.130.32",
            webhookUrl = Mollie.webhookUrl(config)
          ).asJson,
          headers = Mollie.authorization(config.apiKey)
        ).map(_.entity.asString.asObject[Mollie.Payment])
      }).futureValue

      remainingIDs = payments.map(_.id)
      openUrl(payments.head.links("paymentUrl"))

      lapseTime(15 seconds)
      When("The customers pay the 2 payments")
      Then("The payments should be recovered")

      eventually {
        remainingIDs shouldBe empty
      }
    }
  }
}
