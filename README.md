# Mollie extension for akka

The akka-mollie project implements the Mollie API v1 in an actor system.

Configuration:

```
#!json

mollie {
    webhook {
        base-url = "http://yoursite.com"
        interface = "0.0.0.0"
        port = 8080
    }

    api-key = "test_YOUR_KEY"

    client {
        protocol = https
        host = api.mollie.nl
        path = v1
        port = 443
    }
}
```


[Mollie API docs](https://www.mollie.com/nl/docs)

# Versions

## 1.0.0

* Make payments using a registered callback.
* Auto webhook creation. Just few lines of configuration required.
* Recover payments using a registered callback.
* Retrieve all your payments.